# User Account Management Toolkit

The User Account Management Toolkit is a Bash script designed to facilitate the management and inspection of user accounts and groups on a Linux system. This script provides an interactive menu-driven interface, making it easy for system administrators and users to perform a wide range of tasks related to user account management without the need to memorize complex commands.

## Features

The toolkit includes the following options:

1. **Information on an Account**: Provides detailed information about a specified user account, including UID, GID, primary and secondary groups, home directory, and home directory size.
2. **Information on a Group**: Offers details about a specified group, such as group ID, members, and associated users.
3. **Display User Accounts**: Lists all user accounts on the system along with the total count.
4. **Display Service Accounts**: Shows all service accounts, identified by having `/sbin/nologin` or `/bin/false` as their shell, along with a total count.
5. **Manage an Account's Password**: Allows setting password policies for an account, including minimum and maximum age, warning days, and inactivity lockout, as well as enabling or disabling the account.
6. **Display Locked or Disabled Accounts**: Lists user accounts that are either locked or disabled.
7. **Activate, Unlock an Account or Change Its Password**: Provides the option to unlock a locked account, activate a disabled account, or change the account's password.
8. **Quit**: Exits the script.

## Usage

To use the toolkit, follow these steps:

1. Download the script to your Linux system.
2. Make the script executable by running `chmod +x UserAccountManagementToolkit.sh`.
3. Run the script using `./UserAccountManagementToolkit.sh`.
4. Follow the on-screen prompts to select the desired option by entering the corresponding number.
5. Provide any required input, such as the username or group name, when prompted.
6. Press Enter to continue after viewing the output of each command.

## Requirements

This script is designed to run on most Linux distributions. It uses common system utilities (`id`, `grep`, `awk`, `chage`, `passwd`, etc.) that are typically pre-installed.

## Customization

You can customize the script to add more features or modify existing ones according to your needs. The script is divided into functions, making it easy to understand and modify.

## Contribution

Contributions to the User Account Management Toolkit are welcome! Whether it's adding new features, fixing bugs, or improving documentation, your help is appreciated.

## License

This script is provided "as is", without warranty of any kind. You are free to use, modify, and distribute it as per your requirements.

Enjoy managing user accounts more efficiently with the User Account Management Toolkit!
